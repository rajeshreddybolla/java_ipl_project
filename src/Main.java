import Problems.*;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class Main {


    public static void main(String[] args) {

        String MathesPath = "../IPL/Data/matches.csv";
        String DeliveriesPath = "../IPL/Data/deliveries.csv";

        System.out.println(problem1.getMatchesPerYear(MathesPath));
        System.out.println(problem2.MatchesWonPerTeam(MathesPath));
        System.out.println(problem3.Extraruns(MathesPath,DeliveriesPath,"2016"));
        System.out.println(problem4.economicalBowler(MathesPath, DeliveriesPath,"2015"));

        Result result = JUnitCore.runClasses(IplProblems.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());


    }
}