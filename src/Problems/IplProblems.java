package Problems;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        Problems.problem1Test.class,problem2Test.class,problem3Test.class,problem4Test.class
})
public class IplProblems {

}
