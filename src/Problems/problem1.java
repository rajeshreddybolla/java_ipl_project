package Problems;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;


public class problem1 {

    public static Object getMatchesPerYear(String path) {


        HashMap<String, Integer> years = new HashMap<String, Integer>();
        Object obj = null;
        String line = "";
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));

            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                if (years.containsKey(data[1])) {
                    years.put(data[1], count += 1);
                } else {
                    years.put(data[1], count = 1);
                }

            }
            obj = years;


        } catch (FileNotFoundException e) {
            return "FileNotFoundException";
        } catch (IOException e1) {
          e1.printStackTrace();
        }


        return obj.toString();
    }

    public static void main(String[] args) {
        String path =  "../IPL/Data/matches.csv";
        System.out.println(problem1.getMatchesPerYear(path));
    }
}
