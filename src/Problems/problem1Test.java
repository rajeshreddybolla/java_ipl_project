package Problems;

import org.junit.jupiter.api.Assertions;
import org.junit.Test;

//import static Problems.problem1.getMatchesPerYear;
import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

 public class problem1Test {

     String MatchesPath = "../IPL/Data/matches.csv";
     String DeliveriesPath = "../IPL/Data/deliveries.csv";

     Object expectedOutput = "{2009=57, 2008=58, 2017=59, 2016=60, 2015=59, 2014=60, 2013=76, 2012=74, 2011=73, 2010=60}";
     Object actualOutput = problem1.getMatchesPerYear(MatchesPath);



     @Test
    public void TestingProblem1(){

         assertEquals(expectedOutput, actualOutput);
         assertTrue(expectedOutput != actualOutput);
         assertFalse( expectedOutput == actualOutput);
    }

    @Test
   public void WhenIncorrectdatapassing(){

        assertEquals("FileNotFoundException" , problem1.getMatchesPerYear(" "));
        assertEquals(expectedOutput,actualOutput);
     }

     @Test
     public void TestingNull(){
         assertNotNull(actualOutput);
     }





 }





