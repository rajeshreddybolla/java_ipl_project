package Problems;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class problem2 {

    public static Object MatchesWonPerTeam(String path){
        String line = "";
        int count =0;
        HashMap<String, HashMap<String, Integer>> matchesWoned = new HashMap<>();
        Object obj =  null;

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));

            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                String winners = data[10];
                String season = data[1];
                if (!winners.isEmpty()) {

                    if(!(matchesWoned.containsKey(winners))){
                        matchesWoned.put(winners,new HashMap<>());
                    }

                    if((matchesWoned.containsKey(winners))){
                        if(!(matchesWoned.get(winners)).containsKey(season)){
                            matchesWoned.get(winners).put(season,0);
                        }
                        if(((matchesWoned.get(winners)).containsKey(season))){
                            int countofMatches = matchesWoned.get(winners).get(season) + 1;
                            matchesWoned.get(winners).put(season, countofMatches);
                        }
                    }
                }
            }

            obj = matchesWoned;
//            System.out.println(matchesWoned);
        } catch (FileNotFoundException e) {
            return "FileNotFoundException";
        } catch (IOException e) {

        }
            return obj.toString();
    }

    public static void main(String[] args) {
        String Matchespath =  "../IPL/Data/matches.csv";
        System.out.println(MatchesWonPerTeam(" "));

    }

}
