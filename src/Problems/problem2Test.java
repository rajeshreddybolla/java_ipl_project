package Problems;

import org.junit.Test;

import static Problems.problem2.MatchesWonPerTeam;
import static org.junit.jupiter.api.Assertions.*;

 public class problem2Test {

     String MatchesPath = "../IPL/Data/matches.csv";

     Object  expectedOutput= "{Mumbai Indians={2009=5, 2008=7, 2017=12, 2016=7, 2015=10, 2014=7, 2013=13, 2012=10, 2011=10, 2010=11}, Sunrisers Hyderabad={2017=8, 2016=11, 2015=7, 2014=6, 2013=10}, Pune Warriors={2013=4, 2012=4, 2011=4}, Rajasthan Royals={2009=6, 2008=13, 2015=7, 2014=7, 2013=11, 2012=7, 2011=6, 2010=6}, Kolkata Knight Riders={2009=3, 2008=6, 2017=9, 2016=8, 2015=7, 2014=11, 2013=6, 2012=12, 2011=8, 2010=7}, Royal Challengers Bangalore={2009=9, 2008=4, 2017=3, 2016=9, 2015=8, 2014=5, 2013=9, 2012=8, 2011=10, 2010=8}, Gujarat Lions={2017=4, 2016=9}, Rising Pune Supergiant={2017=10}, Kochi Tuskers Kerala={2011=6}, Kings XI Punjab={2009=7, 2008=10, 2017=7, 2016=4, 2015=3, 2014=12, 2013=8, 2012=8, 2011=7, 2010=4}, Deccan Chargers={2009=9, 2008=2, 2012=4, 2011=6, 2010=8}, Delhi Daredevils={2009=10, 2008=7, 2017=6, 2016=7, 2015=5, 2014=2, 2013=3, 2012=11, 2011=4, 2010=7}, Rising Pune Supergiants={2016=5}, Chennai Super Kings={2009=8, 2008=9, 2015=10, 2014=10, 2013=12, 2012=10, 2011=11, 2010=9}}";

     Object actualOutput= MatchesWonPerTeam(MatchesPath);

     @Test
    public void testingProblem2(){
         assertEquals( expectedOutput,actualOutput);
//       assertSame ( expectedOutput, problem2.MatchesWonPerTeam(MatchesPath));
         assertNotSame(expectedOutput, actualOutput);
         assertFalse( expectedOutput == actualOutput);

    }
     @Test
     public void WhenIncorrectdatapassing(){

         assertEquals("FileNotFoundException" , problem2.MatchesWonPerTeam( ""));
//         assertEquals(expectedOutput,actualOutput);
     }

     @Test
     public void TestingNull(){
         assertNotNull(actualOutput);
     }

 }