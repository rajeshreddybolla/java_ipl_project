package Problems;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class problem3 {

    public static Object Extraruns(String pathMatches , String pathDeliveries , String year){

        List RequiredYearId =  new ArrayList();
        HashMap<String, Integer> extraRunsPerTeam = new HashMap<String, Integer>();
        Object obj = null;
        String line = "";
        int count =0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(pathMatches));
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                String matchId = data[0];
                String season = data[1];

                if (season.equals(year)){
                    RequiredYearId.add(matchId);
                }
            }



            BufferedReader br2 = new BufferedReader(new FileReader(pathDeliveries));

            br2.readLine();
            while ((line = br2.readLine()) != null) {
                String[] data = line.split(",");
                String DeliverMatchID = data[0];
                String bowlingTeam = data[3];
                int extraRuns = Integer.parseInt(data[16]);

                if(RequiredYearId.contains(DeliverMatchID)){
                    if(extraRunsPerTeam.containsKey(bowlingTeam)){
                        extraRunsPerTeam.put(bowlingTeam, extraRunsPerTeam.get(bowlingTeam)+ extraRuns);
                    }else {
                        extraRunsPerTeam.put(bowlingTeam, extraRuns);
                    }
                }

            }

        } catch (FileNotFoundException e) {
            return "FileNotFoundException";
        } catch (IOException e) {

        }
//        System.out.println(extraRunsPerTeam);
        obj = extraRunsPerTeam;
        return obj.toString();
    }

    public static void main(String[] args) {
        String pathMatches=  "../IPL/Data/matches.csv";
        String pathDeliveries = "../IPL/Data/deliveries.csv";
        String year = "2016";
        System.out.println(Extraruns(pathMatches,pathDeliveries, year ));
    }

}
