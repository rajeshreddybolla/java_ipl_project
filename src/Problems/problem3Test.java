package Problems;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

 public class problem3Test {

     String MatchesPath = "../IPL/Data/matches.csv";
     String DeliveriesPath = "../IPL/Data/deliveries.csv";
     Object  expectedOutput = "{Gujarat Lions=98, Mumbai Indians=102, Sunrisers Hyderabad=107, Kings XI Punjab=100, Delhi Daredevils=106, Rising Pune Supergiants=108, Kolkata Knight Riders=122, Royal Challengers Bangalore=156}";

     Object expectedOutput2= "{Mumbai Indians=124, Sunrisers Hyderabad=108, Kings XI Punjab=116, Delhi Daredevils=100, Rajasthan Royals=148, Kolkata Knight Riders=117, Royal Challengers Bangalore=114, Chennai Super Kings=99}";

     Object actualOutput = problem3.Extraruns(MatchesPath, DeliveriesPath, "2016");

    @Test
   public void TestingProblem3(){
        assertEquals( expectedOutput,actualOutput);
    }

    @Test
    public void TestingForyear2015(){
        assertEquals(expectedOutput2,problem3.Extraruns(MatchesPath,DeliveriesPath,"2015"));
    }

     @Test
     public void WhenIncorrectdatapassing(){
         assertEquals("FileNotFoundException" , problem3.Extraruns(MatchesPath,""," "));
     }

     @Test
     public void WhenNoYearPassed(){
         assertEquals("{}" , problem3.Extraruns(MatchesPath,DeliveriesPath," "));
     }

     @Test
     public void TestingNull(){
         assertNotNull(actualOutput);
     }
}