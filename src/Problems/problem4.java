package Problems;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


public class problem4 {

    public static Object economicalBowler(String MatchesPath , String deliveriesPath ,String year){
        List RequiredMatchId = new ArrayList();
        if(year == " "){
            return " ";
        }
        HashMap<String, Integer> runs = new HashMap<String, Integer>();
        HashMap<String, Integer> overs = new HashMap<String, Integer>();
        HashMap<String, Float> economy = new HashMap<String, Float>();
        Object obj = null;
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(MatchesPath));
            br.readLine();
            while((line = br.readLine()) != null){
                String[] data = line.split(",");
               //logic
                String matchId  = data[0];
                String season = data[1];

                if(season.equals(year)){
                    RequiredMatchId.add(matchId);
                }
            }

            BufferedReader br2 = new BufferedReader(new FileReader(deliveriesPath));
            br2.readLine();
            while((line = br2.readLine()) != null) {
                String[] data = line.split(",");
                String DeliverMatchID = data[0];
                String bowler = data[8];
                String runsgiven = data[17];
                String balls = data[5];
//
                if (RequiredMatchId.contains(DeliverMatchID)) {
                    if (runs.containsKey(bowler)) {
                        runs.put(bowler, runs.get(bowler )+ Integer.parseInt(runsgiven));
                    } else {
                        runs.put(bowler, Integer.parseInt(runsgiven));
                    }
                }

                if (RequiredMatchId.contains(DeliverMatchID)) {
                    if (balls.contains("6")) {
                        if (overs.containsKey(bowler)) {
                            overs.put(bowler, overs.get(bowler) + 1);
//                            System.out.println(overs.put(bowler, overs.get(bowler) + 1));
                        } else {
                            overs.put(bowler, 1);
                        }
                    }
                }
            }

                for(String val : overs.keySet() ){
                    if(runs.containsKey(val)){
                        economy.put(val , (float) (Math.floor(runs.get(val).floatValue() / overs.get(val).floatValue()*100)/100));
                    }
                }


                Set<Map.Entry<String ,Float>> entryOfData = economy.entrySet();
                List<Map.Entry<String ,Float>> listofData = new ArrayList<>(entryOfData);
                List<Map.Entry<String ,Float>> list = new ArrayList<>();

                listofData.sort((data1, data2) -> data1.getValue().compareTo(data2.getValue()));
                for(int index = 0; index<= 10;index++){
//                    System.out.println(listofData.get(index));
                    list.add(index, listofData.get(index));
                }
                obj = list;

        } catch (FileNotFoundException e) {
            return "FileNotFoundException";
        } catch (IOException e) {

        }
        return obj.toString();
    }

    public static void main(String[] args) {
        String MathesPath = "../IPL/Data/matches.csv";
        String DeliveriesPath = "../IPL/Data/deliveries.csv";
        String year = "2015";
        System.out.println(economicalBowler(MathesPath , DeliveriesPath , " "));
    }
}

