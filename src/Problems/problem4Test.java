package Problems;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

 public class problem4Test {


     String MatchesPath = "../IPL/Data/matches.csv";
     String DeliveriesPath = "../IPL/Data/deliveries.csv";
     Object  expectedOutput = "[RN ten Doeschate=4.0, J Yadav=4.14, V Kohli=5.0, R Ashwin=5.87, S Nadeem=6.14, Parvez Rasool=6.2, MC Henriques=6.56, Z Khan=6.62, M Vijay=7.0, GB Hogg=7.04, MA Starc=7.11]";
     Object actualOutput = problem4.economicalBowler(MatchesPath, DeliveriesPath,"2015");

     Object expectedOutput2 = "[N Rana=3.0, MR Marsh=5.0, YK Pathan=5.5, JW Hastings=6.16, S Gopal=6.33, CH Gayle=6.5, BCJ Cutting=6.66, A Zampa=6.88, Mustafizur Rahman=7.0, KS Williamson=7.0, R Vinay Kumar=7.22]";

     @Test
     public void TestingProblem4(){
        assertEquals( expectedOutput,actualOutput);
    }

     @Test
     public void TestingForyear2016(){
         assertEquals(expectedOutput2,problem4.economicalBowler(MatchesPath,DeliveriesPath,"2016"));
     }

     @Test
     public void WhenIncorrectdatapassing(){
         assertEquals("FileNotFoundException" , problem4.economicalBowler(MatchesPath,"","2015"));
     }

     @Test
     public void WhenNoYearPassed(){
         assertEquals(" " , problem4.economicalBowler(MatchesPath,DeliveriesPath," "));
     }

     @Test
     public void TestingNull(){
         assertNotNull(actualOutput);
     }

}